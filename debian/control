Source: golang-github-bettercap-gatt
Section: devel
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
	       golang-github-mgutz-logxi-dev,
	       golang-github-mattn-go-colorable-dev,
	       golang-github-mattn-go-isatty-dev,
	       golang-github-mgutz-ansi-dev
Standards-Version: 4.3.0
Homepage: https://github.com/bettercap/gatt
Vcs-Browser: https://gitlab.com/kalilinux/packages/golang-github-bettercap-gatt
Vcs-Git: https://gitlab.com/kalilinux/packages/golang-github-bettercap-gatt.git
XS-Go-Import-Path: github.com/bettercap/gatt
Testsuite: autopkgtest-pkg-go

Package: golang-github-bettercap-gatt-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-mgutz-logxi-dev
Description: Gatt is a Go package for building Bluetooth Low Energy peripherals
 This package provides a Bluetooth Low Energy GATT implementation.
 Gatt (Generic Attribute Profile) is the protocol used to write BLE
 peripherals (servers) and centrals (clients).
 .
 As a peripheral, you can create services, characteristics, and
 descriptors, advertise, accept connections, and handle requests.
 .
 As a central, you can scan, connect, discover services, and make
 requests.  SETUPgatt supports both Linux and OS X.On Linux: To gain
 complete and exclusive control of the HCI device, gatt uses HCICHANNELUSER
 (introduced in Linux v3.14) instead of HCICHANNELRAW.  Those who must use
 an older kernel may patch in these relevant commits from Marcel Holtmann:
 Bluetooth: Introduce new HCI socket channel for user operation Bluetooth:
 Introduce user channel flag for HCI devices Bluetooth: Refactor raw
 socket filter into more readable code
 .
 Note that because gatt uses HCICHANNELUSER, once gatt has opened the
 device no other program may access it.
